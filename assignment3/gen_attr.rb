module Genattr
  def initialize
    prod_id = 1
    prod_name = "Product"
    qty = 0
    price = 10
  end

  def method_name
    puts "Select one of these :"
    puts "1. Shopkeeper"
    puts "2. Customer"
    puts "3. Exit"
    choice = gets.chomp.to_i
    case choice
    when 1
      puts "Welcome shopkeeper"
      return 1
    when 2
      puts "Welcome Customer"
      return 2
    when 3
      exit
    else
      puts "Wrong choice"
      return 0
    end
    end

end