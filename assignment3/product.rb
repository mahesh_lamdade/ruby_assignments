require_relative  "fileops"
class Product

  def initialize
    @fileops = FileHandle.new
  end

  def add_product(datahash)
    @fileops.save(datahash)
  end

  def del_prod(delid)
    @fileops.remove(delid)
  end

  def edit_product(datahash)
    @fileops.update(datahash)
  end

  def searchbyid(sid)
    @fileops.find_by_id(sid)
  end

end