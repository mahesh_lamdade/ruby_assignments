require_relative "constants"
require_relative "fileops"
class Order

  def avail?(sname)
    result = ""
    available = false
    File.readlines(INVENTORY_FILE).each do |line|
      result = line.split("|").map(&:strip)
      if result.include? sname
        #puts "Found"
        available =  true
        break
      else
        available = false
      end
    end
    available
  end

  def place_order(datahash)
    #puts "Enter product name you want to buy"
    FileHandle.new.empty_datamov_file
    FileHandle.new.gen_order(datahash)
  end


end