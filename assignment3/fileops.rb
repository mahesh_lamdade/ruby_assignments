require_relative "constants"
class FileHandle

  def rowcount
    prod_id = 0
    File.readlines(INVENTORY_FILE).each do |line|
      rows = line.split("|").map(&:strip)
      prod_id = rows[0].to_i
    end
    return prod_id + 1
  end

  def gen_id
    latest_id = 0
    File.open(INVENTORY_FILE,"a+") do|f2|
      if f2.count != 0
        latest_id = rowcount
      else
        latest_id = f2.count + 1
      end
      f2.close
    end
    latest_id
  end

  def save(datahash)
    prod_id = gen_id
    File.open(INVENTORY_FILE,"a+") do|f2|
      f2.puts datahash.values.to_a.unshift("#{prod_id}").join("| ")
      f2.close
    end

  end

  def remove(delid)
    del_id = delid
    empty_datamov_file
    move_unmatched_data_to_datamov_file(del_id)
    move_temp_data_to_inventory
  end

  def update(datahash)
    empty_datamov_file
    move_original_data_to_datamov_file(datahash)
    move_temp_data_to_inventory
  end

  def find_by_id(sid)
    result = ""
    File.readlines(INVENTORY_FILE).each do |line|
      result = line.split("|").map(&:strip)
      if(result.first == sid)
        result = line.delete("|")
        puts
        break
      else
        result = ""
      end
    end
    result
  end

  def show_all
    line = ""
    File.readlines(INVENTORY_FILE).each do |line|
      puts line.delete("|")
    end
  end

  def find_by_name(sname)
    result = ""
    data = ""
    File.readlines(INVENTORY_FILE).each do |line|
      result = line.split("|").map(&:strip)
      if result.include?(sname)
        data = line.delete("|")
        break
      else
        data = ""
      end
    end
    data
  end

  def empty_datamov_file
    File.open(DATA_MOV_FILE, "wb") do |input|
    end
  end

  def move_unmatched_data_to_datamov_file(del_id)
    count = 0
    File.open(INVENTORY_FILE, "r"){|file|
    file.each_line do |line|
      if line[0]!= del_id.to_s
        File.open(DATA_MOV_FILE, "a+"){|filetemp|
            line_no=(count).to_s
            filetemp.puts line
          }
      end
    count=count+1
    end
    }
  end

  def update_inventory_after_buy(prod_id,upd_qty)
    #new_qty = datahash.values.to_a.last
    count = 0
    result = ""
    File.open(INVENTORY_FILE, "r"){|file|
    file.each_line do |line|
      result = line.split("|").map(&:strip)
      if result.first == prod_id && result[3].to_i!=0
        #new_qty = (result[3].to_i - 1).to_s
        new_data = result[0] + "| " + result[1] + "| " + result[2] + "| " + upd_qty.to_s
      File.open(DATA_MOV_FILE, "a+"){|filetemp|
        filetemp.puts (new_data)
      }
        else
      File.open(DATA_MOV_FILE, "a+"){|filetemp|
        line_no=(count).to_s
        filetemp.puts line
      }
      end
      count=count+1
      end
    }
  end

  def gen_order(datahash)
    prod_to_buy = datahash["prod_name"]
    cust_name = datahash["cust_name"]
    cred_card = datahash["cred_card"]
    cred_cvv = datahash["cvv_number"]
    new_qty = datahash["quantity"]
    result = ""
    prod_details = ""
    prod_qty = 0
    new_id = 0
    new_details = ""
    count = 0
    File.readlines(INVENTORY_FILE).each do |line|
      result = line.split("|").map(&:strip)
      if(result[1] == prod_to_buy && result[3].to_i != 0)
        new_details = datahash.values.unshift(result.first).join("| ")
        puts new_details
        break
      else
        puts "Product not available"
      end
    end
      if prod_details != ''
        File.open(ORDERS_FILE,"a+") do|f2|
        f2.puts new_details
      end
    end
     new_inventory = prod_details.split("|").map(&:strip)
    update_inventory_after_buy(new_inventory.first,prod_qty)
    move_temp_data_to_inventory
  end

  def move_original_data_to_datamov_file(datahash)
    new_qty = datahash.values.to_a.last
    count = 0
    result = ""
    File.open(INVENTORY_FILE, "r"){|file|
    file.each_line do |line|
      result = line.split("|").map(&:strip)
      if result.first == datahash.values.to_a.first
      File.open(DATA_MOV_FILE, "a+"){|filetemp|
        line_no=(count).to_s
        filetemp.puts datahash.values.to_a.join("| ")
      }
        else
      File.open(DATA_MOV_FILE, "a+"){|filetemp|
        line_no=(count).to_s
        filetemp.puts line
      }
      end
      count=count+1
      end
    }
  end

  def move_temp_data_to_inventory
    File.open(DATA_MOV_FILE, "rb") do |input|
      File.open(INVENTORY_FILE, "wb") do |output|
        while buff = input.read(4096)
        output.write(buff)
        output.close
        end
      end
    end
  end

end