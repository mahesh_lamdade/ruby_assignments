string_1 = "I am learning ruby Language"
string_2 = "why ? coz it's cool and i like it"
puts "Q1. Make string_1 capital case(not uppercase)"
puts string_1.capitalize # capitalize string_1
puts

puts "Q2. Make string_1 lower case"
puts string_1.downcase #making string_1 lowercase
puts

puts "Q3. Change the string_1's each letter's case to opposite case."
puts string_1.swapcase #changing current case to opposite one
puts

puts "Q4. Print string_1's characters count"
puts string_1.length #count of words in string_1
puts

puts "Q5. Check if string_1 contains word ruby"
puts string_1.include? "ruby" #find if ruby occurs in string_1
puts

puts "Q6. Split string_2 by '?'"
puts string_2.split("?") #splitting the string by using ?
puts

puts "Q7. Concat string_1 and string_2"
concstr = string_1 + string_2
puts concstr
puts

puts "Q8. Concatenated string, change from 'I' to 'We' and make capital case."
concstr['i']='we'
concstr=concstr.gsub(/I/, 'we')
concstr=concstr.gsub('am', 'are')
puts concstr.capitalize
puts

puts "Q9. Convert string_1 to symbol"
puts string_1.to_sym  #converting string_1 to symbol
puts

puts "Q10. List methods available on strings"
puts "str".methods
puts

puts "Q11. Print strings' length difference"
puts string_1.length - string_2.length #returning differences between string length
puts

puts "Q12. Convert `nil` to array, string, float."
puts nil.to_s #converting nil to string
puts nil.to_a #convert nil to array
puts nil.to_f #convert nil to float
puts

puts "Q13. Write a method which tells if number is even or odd?"
def oddeven(num)
  if num % 2 == 0
    "Even"
  else
    "Odd"
  end
end
puts oddeven(101)
