require 'csv'
FILE_NAME = 'marathi_books.csv'
class ClassGenerator
  attr_accessor :klass
  def initialize(file_name)
    @file_name = file_name
  end

  def generate_class
    attrs = class_attributes
    @klass = Object.const_set class_name, Class.new
    @klass.class_eval do
      attr_accessor *attrs
      def initialize(attributes)
        attributes.each do |attr, value|
          send("#{attr}=", value)
        end
      end
    end
  end

  def to_a
    array = []
    CSV.foreach(FILE_NAME, headers: true) do |row|
      array.push klass.new(row.to_hash)
    end
    array
  end

  def class_name
    @file_name.split('.').first.gsub(/s$/,'').split('_').map(&:capitalize).join('')
  end

  def class_attributes
    CSV.table(FILE_NAME).headers
  end
end


cl = ClassGenerator.new(FILE_NAME)
cl.generate_class
cl.to_a