puts "Q1. Print 'Hello World' 10 times"
10.times do
  puts "hello world"
end
puts

puts "Q2. Print number from 30 to 40"
(30..40).each{ |n| puts n}
puts

puts "Q3. Combine array_1 & array_2 and make elements it uniq"
array_1 = [2, 4, 6, 8, 10]
array_2 = [1, 5, 6, 8, 11, 12]
conc_arr = array_1 + array_2
uniqlist = conc_arr.uniq
puts uniqlist
puts

puts "Q4. Combine array_1 & array_2 and keep only even elements"
evenarr = conc_arr.dup
#evenarr.each { |value| evenarr.pop(value) if value.to_f%2!=0}
puts evenarr.delete_if{ |n| n.to_f%2 != 0}
puts

puts "Q5. Combine array_1 & array_2 and delete elements if greater than 8"
grarr = conc_arr.dup
puts grarr.select{ |n| n > 8 }
puts

puts "Q6. array_1 make cubes of all elements and add them"
cubearr = array_1.dup
sum = 0
sum = (cubearr).map{|i| i**3 }.inject(sum,&:+)
#sum = cubearr.inject { |sum, item| sum + item**3 }
puts sum
puts

puts "Q7. Combine array_1 & array_2 and find index of '8'"
indexeight = array_1.each_index.select{|i| array_1[i] == 8}
puts indexeight
puts

puts "Q8. array_1: add 5 to each element"
addfive = (array_1).map{ |i| i = i+5 }
puts addfive
puts

puts "Q9. combine hash_1 & hash_2"
hash_1 = {a: 'a', b: 'b', c: 'c', d: 'd', e: 'e'}
hash_2 = {x: '10', y: '20', z: '30'}
hash_3 = hash_1.dup
hash_4 = hash_2.dup
puts hash_3.merge(hash_4)
puts

puts "Q10. Replace values of hash_1 with array_1"
replaced_hash = hash_1.each_with_index {|(k,v),index| hash_1[k]=array_1.at(index) }
puts replaced_hash
puts

puts "Q11. Make sum of integer values of hash_2"
sumhash=hash_2.map { |k,v| v =  v.to_i }
#sumhash = hash_2.values.inject{ |a, b| a + b }
puts sumhash.inject(:+)
puts

puts "Q12. Make upcase of all values of hash_1"
uphash = hash_1.dup
puts uphash.map{ |a, b| b.upcase }
puts

puts "Q.14 write a method: 'compose' which takes two procs and returns a new proc which, when called, calls the first proc and passes its result into the second proc. both of the proc arguments takes a number argument"
def compose(n,square = Proc.new { |n| n * n } ,double = Proc.new { |n| n * 2 } )
 n=double.call(n)
 square.call(n)
end
puts compose(5)
puts

puts "Q15. Write a lambda which takes first_name and last_name and returns full_name."
lambex = lambda { |first_name,second_name | "#{first_name} #{second_name}" }
puts lambex.call("Alex","Bradman")
puts

