Gem::Specification.new do |s|
  s.name        = 'Store'
  s.version     = '0.0.8'
  s.date        = '2017-02-24'
  s.summary     = "Store"
  s.description = "Shopkeeper and customer store"
  s.authors     = ["Nick Quaranto"]
  s.email       = 'nick@quaran.to'
  s.executables << 'hola'
  s.files       = ["lib/assignment_3.rb","lib/customer.rb","lib/fileops.rb","lib/gen_attr.rb","lib/inventory.txt","lib/orders.txt","lib/orders.rb","lib/product.rb","lib/sample_temp.txt","lib/shop_keeper.rb","lib/validate_input.rb"]
  s.homepage    =
    'http://rubygems.org/gems/store'
  s.license       = 'MIT'
end