require_relative "gen_attr"
require_relative "shop_keeper"
require_relative "customer"

class Selection
  include Genattr
  def selectopt
    r = method_name
    case r
    when 1
      Shopkeeper.new.shopopt
    when 2
      Customer.new.custopt
    when 0
      puts "Wrong selection"
    end
  end


end
Selection.new.selectopt
