require_relative "validate_input"
require_relative "product"
class Shopkeeper

  def initialize
    prod_name = ""
    price = 0
    qty = 0
    @validint = false
    @valid_input = Validate.new
    @product = Product.new
  end

  def add_product
    product_hash = {}
    attrs = ["product_name","product_price","quantity"]
    attrs.each{ |attr| puts "Enter #{attr.gsub("_",' '.capitalize)}" ; product_hash[attr] = gets.chomp}
    puts product_hash
    @product.add_product(product_hash)
  end

  def del_product
    del_id = 0
    puts "Enter Product Id to be deleted"
    del_id = gets.chomp
    result = ""
    count = 0
    if @valid_input.number?(del_id)
      @product.del_prod(del_id)
    else
      puts "Invalid Input"
      del_prod
    end
  end

  def edit_product
    attrs = ["product_id_to_edit","new_name","new_price","new_quantity"]
    edit_prod_hash = {}
    attrs = ["product_name","product_price","quantity"]
    attrs.each{ |attr| puts "Enter #{attr.gsub("_",' '.capitalize)}" ; edit_prod_hash[attr] = gets.chomp}
    puts edit_prod_hash
    @product.edit_product(edit_prod_hash)

  end
  def search_product
    srinp = ""
    puts "Product Id to search"
    srinp = gets.chomp
    if @valid_input.number?(srinp)
      result = @product.searchbyid(srinp)
      if result.empty?
        puts "Not found"
      else
        puts "Found"
        print result
      end
    else
      puts "Wrong input"
    end
  end

  def shopopt
    puts "What do you want to do:"
    puts "1. Add product"
    puts "2. Delete product"
    puts "3. Edit product"
    puts "4. Search product"
    puts "5. Exit"
    shop_prod = gets.chomp.to_i
    case shop_prod
    when 1
      add_product
    when 2
      del_product
    when 3
      edit_product
    when 4
      search_product
    when 5
      exit
    else
      puts "wrong choice"
    end
  end
end